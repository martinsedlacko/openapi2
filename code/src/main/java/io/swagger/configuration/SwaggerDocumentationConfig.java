package io.swagger.configuration;

import com.fasterxml.classmate.TypeResolver;
import io.swagger.model.entity.Appointment;
import io.swagger.model.entity.Client;
import io.swagger.model.entity.Doctor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-04-09T12:53:09.559+02:00")

@Configuration
public class SwaggerDocumentationConfig {

    @Autowired
    private TypeResolver typeResolver;

    ApiInfo apiInfo() {
        return new ApiInfoBuilder()
            .title("My REST API")
            .description("Description of API")
            .license("License of API")
            .licenseUrl("http://unlicense.org")
            .termsOfServiceUrl("https://aardwark.com")
            .version("1.0")
            .contact(new Contact("","", "martin.sedlacko@aardwark.com"))
            .build();
    }

    @Bean
    public Docket customImplementation(){
        return new Docket(DocumentationType.SWAGGER_2)
                .additionalModels(typeResolver.resolve(Appointment.class),
                        typeResolver.resolve(Client.class),
                        typeResolver.resolve(Doctor.class))
                .select()
                    .apis(RequestHandlerSelectors.basePackage("io.swagger.api"))
                    .build()
                .directModelSubstitute(org.threeten.bp.LocalDate.class, java.sql.Date.class)
                .directModelSubstitute(org.threeten.bp.OffsetDateTime.class, java.util.Date.class)
                .apiInfo(apiInfo());
    }

}
