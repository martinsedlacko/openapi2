package io.swagger.api;

import io.swagger.model.entity.Doctor;
import io.swagger.model.dto.DoctorDTO;
import io.swagger.api.response.ResponseMessage;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.*;
import io.swagger.repositories.DoctorRepository;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.*;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-04-09T12:53:09.559+02:00")

@Controller
public class DoctorApiController implements DoctorApi {

    private static final Logger log = LoggerFactory.getLogger(DoctorApiController.class);
    public static final String DELETED_SUCCESSFULLY = "Doctor deleted successfully";

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    private final DoctorRepository doctorRepository;

    private final ModelMapper modelMapper;

    @org.springframework.beans.factory.annotation.Autowired
    public DoctorApiController(ObjectMapper objectMapper, HttpServletRequest request, ModelMapper modelMapper,
                               DoctorRepository doctorRepository) {
        this.objectMapper = objectMapper;
        this.request = request;
        this.modelMapper = modelMapper;
        this.doctorRepository = doctorRepository;
    }

    public ResponseEntity<Doctor> addDoctorUsingPOST(@ApiParam(value = "" ,required=true )  @Valid @RequestBody DoctorDTO doctorDTO) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            return new ResponseEntity<>(
                    this.doctorRepository.save(convertToEntity(doctorDTO)),
                    HttpStatus.OK
            );
        }
        return new ResponseEntity<Doctor>(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    }

    public ResponseEntity<ResponseMessage> deleteDoctorByIdUsingDELETE(@NotNull @ApiParam(value = "id of doctor to delete", required = true) @Valid @RequestParam(value = "id", required = true) Long id) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            if(this.doctorRepository.exists(id)){
                this.doctorRepository.delete(id);
                return new ResponseEntity<>(new ResponseMessage(DELETED_SUCCESSFULLY, "200"),
                        HttpStatus.OK);
            }else
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<ResponseMessage>(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    }

    public ResponseEntity<Doctor> getDoctorByIdUsingGET(@NotNull @ApiParam(value = "ID of doctor to get", required = true) @Valid @RequestParam(value = "id", required = true) Long id) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            if(doctorRepository.exists(id))
                return new ResponseEntity<>(this.doctorRepository.findOne(id), HttpStatus.OK);
            else
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<Doctor>(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    }

    public ResponseEntity<Doctor> updateDoctorUsingPUT(@ApiParam(value = "" ,required=true )  @Valid @RequestBody Doctor doctor) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            if(doctorRepository.exists(doctor.getId()))
                return new ResponseEntity<>(this.doctorRepository.save(doctor), HttpStatus.OK);
            else
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Doctor>(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    }

    private Doctor convertToEntity(DoctorDTO doctorDTO){
        return modelMapper.map(doctorDTO, Doctor.class);
    }

}
