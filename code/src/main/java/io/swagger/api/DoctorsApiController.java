package io.swagger.api;

import io.swagger.model.entity.Doctor;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.repositories.DoctorRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-04-09T12:53:09.559+02:00")

@Controller
public class DoctorsApiController implements DoctorsApi {

    private static final Logger log = LoggerFactory.getLogger(DoctorsApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    private final DoctorRepository doctorRepository;

    @org.springframework.beans.factory.annotation.Autowired
    public DoctorsApiController(ObjectMapper objectMapper, HttpServletRequest request, DoctorRepository doctorRepository) {
        this.objectMapper = objectMapper;
        this.request = request;
        this.doctorRepository = doctorRepository;
    }

    public ResponseEntity<List<Doctor>> getAllDoctorsUsingGET() {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            return new ResponseEntity<>(this.doctorRepository.findAll(), HttpStatus.OK);
        }
        return new ResponseEntity<List<Doctor>>(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    }

}
