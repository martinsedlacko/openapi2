package io.swagger.api.exceptions;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-04-09T12:53:09.559+02:00")

public class NotFoundException extends ApiException {

    private int code;

    public NotFoundException (int code, String msg) {
        super(code, msg);
        this.code = code;
    }
}
