package io.swagger.api;

import io.swagger.model.entity.Client;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.repositories.ClientRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-04-09T12:53:09.559+02:00")

@Controller
public class ClientsApiController implements ClientsApi {

    private static final Logger log = LoggerFactory.getLogger(ClientsApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    private final ClientRepository clientRepository;

    @org.springframework.beans.factory.annotation.Autowired
    public ClientsApiController(ObjectMapper objectMapper, HttpServletRequest request, ClientRepository clientRepository) {
        this.objectMapper = objectMapper;
        this.request = request;
        this.clientRepository = clientRepository;
    }

    public ResponseEntity<List<Client>> getAllClientsUsingGET() {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            return new ResponseEntity<List<Client>>(this.clientRepository.findAll(),
                    HttpStatus.OK);
        }

        return new ResponseEntity<List<Client>>(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    }

}
