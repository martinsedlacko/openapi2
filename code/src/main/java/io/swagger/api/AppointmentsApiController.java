package io.swagger.api;

import io.swagger.model.entity.Appointment;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.repositories.AppointmentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-04-09T12:53:09.559+02:00")

@Controller
public class AppointmentsApiController implements AppointmentsApi {

    private static final Logger log = LoggerFactory.getLogger(AppointmentsApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    private final AppointmentRepository appointmentRepository;
    @org.springframework.beans.factory.annotation.Autowired
    public AppointmentsApiController(ObjectMapper objectMapper, HttpServletRequest request,
                                     AppointmentRepository appointmentRepository) {
        this.objectMapper = objectMapper;
        this.request = request;
        this.appointmentRepository = appointmentRepository;
    }

    public ResponseEntity<List<Appointment>> getAllAppointmentsUsingGET() {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            List<Appointment> appointmentList = this.appointmentRepository.findAll();
            return new ResponseEntity<List<Appointment>>(appointmentList, HttpStatus.NOT_IMPLEMENTED);
        }
        return new ResponseEntity<List<Appointment>>(HttpStatus.NOT_IMPLEMENTED);
    }
}
