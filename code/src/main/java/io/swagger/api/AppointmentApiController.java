package io.swagger.api;

import io.swagger.model.entity.Appointment;
import io.swagger.model.dto.AppointmentDTO;
import io.swagger.api.response.ResponseMessage;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.*;
import io.swagger.repositories.AppointmentRepository;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.*;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-04-09T12:53:09.559+02:00")

@Controller
public class AppointmentApiController implements AppointmentApi {

    private static final Logger log = LoggerFactory.getLogger(AppointmentApiController.class);
    public static final String APPOINTMENT_DELETED_SUCCESSFULLY = "Appointment deleted successfully";

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    private final AppointmentRepository appointmentRepository;

    private final ModelMapper modelMapper;

    @org.springframework.beans.factory.annotation.Autowired
    public AppointmentApiController(ObjectMapper objectMapper, HttpServletRequest request,
                                    AppointmentRepository appointmentRepository, ModelMapper modelMapper) {
        this.objectMapper = objectMapper;
        this.modelMapper = modelMapper;
        this.request = request;
        this.appointmentRepository = appointmentRepository;
    }

    public ResponseEntity<Appointment> addAppointmentUsingPOST(@ApiParam(value = "" ,required=true )  @Valid @RequestBody AppointmentDTO appointment) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            return new ResponseEntity<Appointment>(appointmentRepository.save(convertToEntity(appointment)), HttpStatus.OK);
        }

        return new ResponseEntity<Appointment>(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    }

    public ResponseEntity<ResponseMessage> deleteAppointmentByIdUsingDELETE(@NotNull @ApiParam(value = "ID of appointment to get", required = true) @Valid @RequestParam(value = "id", required = true) Long id) {
        String accept = request.getHeader("Accept");
        if(!this.appointmentRepository.exists(id))
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        if (accept != null && accept.contains("application/json")) {
            this.appointmentRepository.delete(id);
            return new ResponseEntity<ResponseMessage>(new ResponseMessage(APPOINTMENT_DELETED_SUCCESSFULLY, "200"), HttpStatus.OK);
        }

        return new ResponseEntity<ResponseMessage>(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    }

    public ResponseEntity<Appointment> getAppointmentByIdUsingGET(@NotNull @ApiParam(value = "ID of appointment to get", required = true) @Valid @RequestParam(value = "id", required = true) Long id) {
        String accept = request.getHeader("Accept");

        if(!this.appointmentRepository.exists(id))
            return new ResponseEntity<Appointment>(HttpStatus.NOT_FOUND);
        if (accept != null && accept.contains("application/json")) {
            return new ResponseEntity<>(this.appointmentRepository.findOne(id), HttpStatus.OK);
        }

        return new ResponseEntity<Appointment>(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    }

    public ResponseEntity<Appointment> updateAppointmentUsingPUT(@ApiParam(value = "" ,required=true )  @Valid @RequestBody Appointment appointment) {
        String accept = request.getHeader("Accept");

        if(!this.appointmentRepository.exists(appointment.getId()))
            return new ResponseEntity<Appointment>(HttpStatus.NOT_FOUND);
        if (accept != null && accept.contains("application/json")) {
            return new ResponseEntity<>(this.appointmentRepository.save(appointment), HttpStatus.OK);
        }

        return new ResponseEntity<Appointment>(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    }

    private Appointment convertToEntity(AppointmentDTO appointmentDTO){
        return modelMapper.map(appointmentDTO, Appointment.class);
    }

}
