package io.swagger.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.ApiParam;
import io.swagger.model.entity.Client;
import io.swagger.model.dto.ClientDTO;
import io.swagger.api.response.ResponseMessage;
import io.swagger.repositories.ClientRepository;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-04-09T12:53:09.559+02:00")

@Controller
public class ClientApiController implements ClientApi {

    private static final Logger log = LoggerFactory.getLogger(ClientApiController.class);
    public static final String DELETED_SUCCESSFULLY = "Client deleted successfully";

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    private final ClientRepository clientRepository;

    private final String NOT_FOUND_MESSAGE = "Client Not Found";

    public final ModelMapper modelMapper;

    @org.springframework.beans.factory.annotation.Autowired
    public ClientApiController(ObjectMapper objectMapper, HttpServletRequest request, ClientRepository clientRepository,
                               ModelMapper modelMapper) {
        this.objectMapper = objectMapper;
        this.request = request;
        this.clientRepository = clientRepository;
        this.modelMapper = modelMapper;
    }

    public ResponseEntity<Client> addClientUsingPOST(@ApiParam(value = "" ,required=true )  @Valid @RequestBody ClientDTO client) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            return new ResponseEntity<>(
                    this.clientRepository.save(convertToEntity(client)),
                    HttpStatus.OK
            );
        }

        return new ResponseEntity<Client>(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    }

    public ResponseEntity<ResponseMessage> deleteClientByIdUsingDELETE(@NotNull @ApiParam(value = "id", required = true)
                                                                       @Valid @RequestParam(value = "id", required = true) Long id) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            if(!this.clientRepository.exists(id))
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);

            this.clientRepository.delete(id);

            return new ResponseEntity<>(new ResponseMessage(DELETED_SUCCESSFULLY, "200"),
                    HttpStatus.OK);
        }
        return new ResponseEntity<ResponseMessage>(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    }

    public ResponseEntity<Client> getClientByIdUsingGET(@NotNull @ApiParam(value = "ID of client to get", required = true) @Valid @RequestParam(value = "id", required = true) Long id) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            Client client = clientRepository.findOne(id);
            if(client !=  null)
                return new ResponseEntity<Client>(client, HttpStatus.NOT_IMPLEMENTED);
            else
                return new ResponseEntity<Client>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<Client>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Client> updateClientUsingPUT(@ApiParam(value = "client" ,required=true )  @Valid @RequestBody Client client) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            if(this.clientRepository.exists(client.getId())){
                this.clientRepository.save(client);
                return new ResponseEntity<>(client, HttpStatus.OK);
            }else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }
        return new ResponseEntity<Client>(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    }

    private Client convertToEntity(ClientDTO clientDTO){
        return modelMapper.map(clientDTO, Client.class);
    }

}
